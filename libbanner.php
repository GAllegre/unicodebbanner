<?php

/**
 * bbanner
 * Braille graphical banner
 *
 * @author Guillaume Allègre <allegre.guillaume@free.fr>
 * @copyright 2019-2023  Guillaume Allègre <allegre.guillaume@free.fr>
 * @license GNU Affero GPL v.3.0  https://www.gnu.org/licenses/agpl-3.0.en.html
**/

#require_once "portable-utf8.php";

/* explication : les caractères sont codés par colonne : 1 chiffre hexa = 1 colonne
   simple ligne (4 pixels) = mono ; double ligne (8 pixels) = high + low
   pour chacun, bit 0 (haut) à 3 (bas)
  1  ....  
  2  .**.
  4  *..*
  8  ****
  1  *..*  .*. 
  2  *..*  *.*
  4  *..*  *** 
  8  ....  *.*
     CAAC     
     7007  E5E
*/

$charColumns = array(
	'%' => array('mono' => '6DECF6', 'high' =>'CC2B2CC' , 'low' => '3676763' ), //couronne
	// '' => array('mono' => '', 'high' => '', 'low' => ''),
	'z' => array('mono' => '111', 'high' => 'BDBD', 'low' => '0000'), //─
	'r' => array('mono' => '11', 'high' => 'BD', 'low' => '00'), // demi ─
	'x' => array('mono' => '', 'high' => '0000', 'low' => 'BDBD'), //━
	'v' => array('mono' => '', 'high' => '00', 'low' => 'BD'), //demi ━
	'a' => array('mono' => '', 'high' => 'FB5B', 'low' => 'F5AF'), //┌
	'w' => array('mono' => '', 'high' => 'F5AF', 'low' => 'FDAD'), //└
	'c' => array('mono' => '', 'high' => 'F5AF', 'low' => 'DADF'), //┘
	'e' => array('mono' => '', 'high' => 'B5BF', 'low' => 'F5AF'), //┐
	'q' => array('mono' => 'F', 'high' => 'F5AF', 'low' => 'F5AF'), //│
	//
	'-' => array('mono' => '44', 'high' => '000', 'low' => '111'),
	'|' => array('mono' => 'F', 'high' => 'E', 'low' => '7'),
	',' => array('mono' => '0', 'high' => '0', 'low' => '0'), // espace fine
	';' => array('mono' => '000', 'high' => '000', 'low' => '000'), //espace large
	'£' => array('mono' => '0', 'high' => '0', 'low' => '0'), // colonne intercalaire
	"'" => array('mono' => '21', 'high' => '43', 'low' => '00'),
	'.' => array('mono' => '8', 'high' => '0', 'low' => '4'),
	' '	=> array('mono' => '00', 'high' => '00', 'low' => '00'),
	'A' => array('mono' => 'E5E', 'high' => 'C22C', 'low' => '7117'),
	'B' => array('mono' => 'FAC', 'high' => 'EAA4', 'low' => '7443'),
	'C' => array('mono' => '699', 'high' => 'C224', 'low' => '3442'),
	'Ç' => array('mono' => '699', 'high' => 'C224', 'low' => '34C2'),
	'D' => array('mono' => 'F96', 'high' => 'E22C', 'low' => '7443'),
	'E' => array('mono' => 'FB9', 'high' => 'EAA2', 'low' => '7444'),
	'F' => array('mono' => 'F51', 'high' => 'EAA2', 'low' => '7000'),
	'G' => array('mono' => '69D', 'high' => 'C224', 'low' => '3453'),
	'H' => array('mono' => 'F4F', 'high' => 'E00E', 'low' => '7117'),
	'I' => array('mono' => 'F', 'high' => 'E', 'low' => '7'),
	'J' => array('mono' => '487', 'high' => '000E', 'low' => '2443'),
	'K' => array('mono' => 'F69', 'high' => 'E842', 'low' => '7016'),
	'L' => array('mono' => 'F88', 'high' => 'E000', 'low' => '7444'),
	'M' => array('mono' => 'F242F', 'high' => 'E484E', 'low' => '70007'),
	'N' => array('mono' => 'F24F', 'high' => 'E80E', 'low' => '7017'),
	'O' => array('mono' => '696', 'high' => 'C22C', 'low' => '3443'),
	'P' => array('mono' => 'F52', 'high' => 'E22C', 'low' => '7110'),
	'Q' => array('mono' => '6DE', 'high' => 'C22C', 'low' => '3467'),
	'R' => array('mono' => 'F5A', 'high' => 'E22C', 'low' => '7134'),
	'S' => array('mono' => 'AB5', 'high' => '4AA2', 'low' => '2443'),
	'T' => array('mono' => '1F1', 'high' => '2E2', 'low' => '070'),
	'U' => array('mono' => '787', 'high' => 'E00E', 'low' => '3443'),
	'V' => array('mono' => '3C3', 'high' => 'E000E', 'low' => '03430'),
	'W' => array('mono' => '78687', 'high' => 'E000E', 'low' => '36363'),
	'X' => array('mono' => '969', 'high' => '6886', 'low' => '6116'),
	'Y' => array('mono' => '1E1', 'high' => '6886', 'low' => '4310'),
	'Z' => array('mono' => 'DB9', 'high' => '22A6', 'low' => '6544'),
	'0' => array('mono' => '696', 'high' => 'C2AC', 'low' => '3543'),
	'1' => array('mono' => '2F', 'high' => '84E', 'low' => '007'),
	'2' => array('mono' => '9DA', 'high' => '42A4', 'low' => '6544'),
	'3' => array('mono' => '9B6', 'high' => '2AA4', 'low' => '2443'),
	'4' => array('mono' => '74E', 'high' => '84E0', 'low' => '3272'),
	'5' => array('mono' => 'BB5', 'high' => 'EAA2', 'low' => '2443'),
	'6' => array('mono' => '6DD', 'high' => 'CAA2', 'low' => '3443'),
	'7' => array('mono' => '953', 'high' => '22A6', 'low' => '6100'),
	'8' => array('mono' => 'FBF', 'high' => '4AA4', 'low' => '3443'),
	'9' => array('mono' => 'BB6', 'high' => 'C22C', 'low' => '4553'),
	'!' => array('mono' => 'B', 'high' => 'E', 'low' => '5'),
	'?' => array('mono' => '296', 'high' => '42A4', 'low' => '0B00'),
	':' => array('mono' => 'A', 'high' => '4', 'low' => '2'),
);


function intercale($text) {
	$output = array();
	foreach (str_split($text) as $char) {
		$output[] = $char ;
		$output[] = '£';
	}
	return $output;
}


function charToColumns($char, $row) {
	global $charColumns;
	
	if (isset($charColumns[$char])) {
		$res = $charColumns[$char][$row];
		if ($res == '') {
			$res = '5A5A';
		}
	} else {
		$res = '5A5A';
	}
	return str_split($res, 1);
}

function textToBrailleSmall($text) {
	return textToBrailleRow($text, 'mono') . "\n";
}

function textToBrailleTall($text) {
	return textToBrailleRow($text, 'high') . "\n"
		. textToBrailleRow($text, 'low') . "\n";
}

function textToBrailleRow($text, $row) {
	$output = '';
	$prevColumn = '';
	$index = 0;

	$tableBraille = array(
		array('00', '01', '02', '03', '04', '05', '06', '07', '40', '41', '42', '43', '44', '45', '46', '47'),
	    array('00', '08', '10', '18', '20', '28', '30', '38', '80', '88', '90', '98', 'A0', 'A8', 'B0', 'B8'),
	);
	foreach (intercale($text) as $character) {
		$charcolumns = charToColumns($character, $row);

		foreach ($charcolumns as $charcolumn) {
			$column = $tableBraille[$index][hexdec($charcolumn)];
			if ($index == 0) {
				$prevColumn = $column;
			} else {
				$braille = sprintf('%02x', hexdec($prevColumn) + hexdec($column));
				$prevColumn = '';
				$brailleUnicode = '0x28' . $braille;
				$brailleChar = mb_chr(hexdec($brailleUnicode), 'UTF-8');
				$output .= $brailleChar;
			}
			$index = 1-$index;
		} // character column
	} // text character
	return $output;
}


function demostring() {
    return strtoupper('abcdefghijklmnopqrstuvwxyz').' 0123456789';
}

