This little dirty script uses the unicode Braille range
to generate old-fashioned banners, like the following:

```
⡮⡆⣇⡀⣇⡀⠀⠘⡜⠰⡱⠸⡸⢸⢕⠀⠀⣗⡄⡮⡆⣚⠅⣟⡁⠀⢰⢵⢸⢕⢸⣋⠀⠀⣗⡄⣟⡁⣇⡀⢎⠆⡗⢼⠰⣩⠀⠀⢹⠁⢎⠆⠀⠸⡸⢐⡫

⡔⢢⢰⠀⠀⡆⠀⠀⠀⢆⡰⢠⠒⡄⡆⢰⢰⠒⡄⠀⢰⣒⠄⡔⢢⠠⣒⠂⣖⡒⠀⠀⡔⢢⢰⠒⡄⣖⡒⠀⠀⣖⡢⢰⣒⠂⡆⠀⢠⠒⡄⣆⢰⢠⠒⠄⠀⠐⡖⢠⠒⡄⠀⢰⠀⡆⢔⡒
⠏⠹⠸⠤⠄⠧⠤⠀⠀⠜⠁⠘⠤⠃⠣⠜⠸⠙⠄⠀⠸⠤⠃⠏⠹⠠⠤⠃⠧⠤⠀⠀⠏⠹⠸⠙⠄⠧⠤⠀⠀⠧⠜⠸⠤⠄⠧⠤⠘⠤⠃⠇⠹⠘⠬⠃⠀⠀⠇⠘⠤⠃⠀⠘⠤⠃⠤⠜
```

They come in 2 variants : small (4 "pixels" height), on one row
and tall (8 "pixels" height), on two rows. 
Final rendering depends on some terminal features and fonts.
This renders quite well in Twitter.

From a standard point of view, it's a very bad use of Braille
codepoints, as it is a pure graphical abuse of the Braille patterns.
Kids, don't do this at home.


Usage:
```
php bbanner.php "all your base are belong to us"
```
or, if you have shebangs and executable script:
```
bbanner.php "shall we play a game?"
```

